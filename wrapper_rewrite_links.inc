<?php
/**
 * @file
 * This code is on probation - copied as-is from wrapper-d6.
 *
 * It's really scary. Letes just throw it at the source and see what happens.
 */

/**
 * Fix up all the links found in the source of an HTML doc.
 *
 * @param string $source
 *   HTML source to be rewritten.
 * @param string $source_path
 *   The documents base_url. includes the wrapper-container
 *   URL path as defined in the profile, and the rest of the requested path.
 *   Need to chop it back (subtract the $instance['path']
 *   to find the relative path.
 * @param string $dest_path
 *   The localized (probaby root-relative) equivalent of that
 *   $source_path. This is the local path that this conent will be served from.
 * @param object $wrapper
 *   Context. Probably an entity.
 *
 * @return string
 *   The rewritten source.
 */
function wrapper_redirect_links($source, $source_path, $dest_path, $wrapper) {

  // For the purpose of rewriting links, the source_path should be flattened
  // to a directory - should finish with '/'
  // source_base is the URL to calculate links from.
  // This should be set upstream if possible.
  if (empty($wrapper->source_base)) {
    $wrapper->source_base = $wrapper->source;
    if (!preg_match('%/$%', $wrapper->source_base)) {
      $wrapper->source_base = dirname($wrapper->source_base) . '/';
    }
  }

  $is_root = ($source_path == $wrapper->source);

  $strings = array(
    '%source_path' => $source_path,
    '%dest_path' => $dest_path,
  );
  /*
  watchdog(__FUNCTION__, '
    Input was from [%source_path], We need to make its links seem as if they
    were relative to destination path (the local path) [%dest_path].',
    $strings, WATCHDOG_DEBUG
  );
  */

  // Why is there no PHP func for resolving URLs?
  // To use callbacks, I need to put extra args into global ... :(
  global $_wrapper_new_path;
  // The localized path of where the current page will be put.
  global $_wrapper_instance;
  // Contains ->source (the root of the mirror).
  $_wrapper_instance = $wrapper;
  // Pre-deconstruct the source parts into an array, it's useful later.
  $_wrapper_instance->base_parts = parse_url($_wrapper_instance->source_base);
  // An empty path is OK, but define it to avoid warnings.
  $_wrapper_instance->base_parts += array(
    'path' => '',
    'host' => '',
    'scheme' => '',
  );

  // When linking further items, our drupal path cannot have a trailing slash
  // but the content we put under it will assume it's there.
  // Put it in if needed.
  // ensures a single trailing slash.
  $_wrapper_instance->path = rtrim($_wrapper_instance->path, '/') . '/';

  // The difference between the wrapper instance root and the current request.
  global $_wrapper_rel_path;
  // The full remote URL of the page being processed.
  global $_wrapper_current_path;
  $_wrapper_current_path = $source_path;
  // Broken down version of that.
  global $_wrapper_current_path_parts;
  $_wrapper_current_path_parts = parse_url($_wrapper_current_path);

  $_wrapper_rel_path = $source_path;

  $strings['%wrapper_current_path'] = $_wrapper_current_path;

  if (!$_wrapper_current_path_parts) {
    drupal_set_message(t('
      There was a problem translating %wrapper_current_path into a valid URL.
      URL rewriting will not proceed
    ', $strings), 'error');
    return $source;
  }

  $_wrapper_new_path = $dest_path;
  /*
  watchdog('wrapper', "
  Rewriting links within the document from '%wrapper_current_path'
  to its new home at '%dest_path'
  ", $strings, WATCHDOG_DEBUG
  );
   */
  $output = $source;

  // Replace all src-s
  // $pattern = "|(<[^>]*src=['\"])([^'\"]+?)(['\"][^>]*>)|i";
  // That dies when it hit an asp.net 'viewstate' field
  // with more that 100k in a form element value.
  // Try to be correct, while avoiding asp.net stupid fields
  // $pattern = "|(<(img|iframe|script)[^>]*src=['\"])([^'\"]+)(['\"][^>]*>)|i";
  // ... failed.
  // A less intensive, less careful or specific version.
  $pattern = "|(src=['\"])([^'\"]+?)(['\"])|i";

  // $match[2] is the URL that needs changing
  // We need to think, so use a callback function.
  $output = preg_replace_callback($pattern, 'wrapper_redirect_url_callback', $output);

  if (is_null($output)) {
    watchdog('wrapper', "
      wrapper_redirect_links failed. This is allegedly because of limits on the
      PHP pcre.backtrack_limit value.
      This is known to be triggered when parsing pages containing asp.net VIEWSTATE forms.
      pcre.backtrack_limit=1000000 may fix this
      ", $strings, WATCHDOG_ERROR
    );
    return "Page extraction failed - see the error logs.";
  }

  // Replace all hrefs-s
  $pattern = "|(href=['\"])([^'\"]+?)(['\"])|i";
  $output = preg_replace_callback($pattern, 'wrapper_redirect_url_callback', $output);

  // And form actions.
  $pattern = "|(action=['\"])([^'\"]+?)(['\"])|i";
  $output = preg_replace_callback($pattern, 'wrapper_redirect_url_callback', $output);

  return $output;
}


/**
 * This is way complex.
 *
 * We have a remote location, possibly a remote subdir, and an actual remote
 * request, which is to be relative to that subdir.
 * We also have a local alias, and the remote rel request is to be rewritten as
 * if it's under the rel path there.
 *
 * Once we've gotten the remote page:
 *
 * Fully-justified external URLs are left alone. - - unless they are actually
 * hard- coded referring to the current site, in which case they are localized
 * temporarily.
 *
 * then the local links are resolved into root-relative ones, fixing all
 * relative links into the actual path.
 *
 * Then the path is compared against the target subdirectory (if any) we are
 * rewriting content for.
 *
 * If it's in, then the subdirectory is removed again to give us the short path
 * that will then be served from under our local alias.
 *
 * Remote pages outside of the mirrored dir get converted into hard, remote URLs
 * back to the source.
 */
function wrapper_redirect_url_callback($matches) {
  // We need to use a bunch of globals to support regexp callbacks.
  // The difference between the wrapper instance root and the current request.
  global $_wrapper_rel_path;
  // The full remote URL of the page being processed.
  global $_wrapper_current_path;
  // Broken down version of that.
  global $_wrapper_current_path_parts;
  // The localized path of where the current page will be put.
  global $_wrapper_new_path;
  global $_wrapper_instance;

  $link = $matches[2];

  // dpm("Found link to '$link'.
  // May need to shift it from being relative to [source_root]
  // '$_wrapper_current_path' (its home context)
  // to being relative to [wrapper_root]'$_wrapper_new_path'
  // (our new home)" );
  // Rewriting local #anchors (with no other path info) is always unwanted.
  // Leave it alone.
  if (substr($link, 0, 1) == '#') {
    return $matches[0];
  }

  // Calculate/resolve the full URL, based on context.
  // This is MORE complex than what we were given,
  // but more correct, so we don't have to guess anything later.
  $link_parts = parse_url(wrapper_rel2abs($link, $_wrapper_current_path));
  // We'll strip OUT the full path later by replacing the source base
  // with the wrapper base path.
  if (!is_array($link_parts)) {
    drupal_set_message(t("Failed to rewrite found URL : '@link'", array('@link' => $link)), 'error');
    return $matches[0];
  }

  // Full in some blank defaults, even if it's empty, just to avoid warnings.
  $link_parts += array(
    'path' => '',
    'host' => '',
    'scheme' => '',
  );

  // Check if link is a JS action
  // If true leave it alone.
  if ($link_parts['scheme'] == 'javascript') {
    return $matches[0];
  }

  if ($link_parts['host'] != $_wrapper_instance->base_parts['host']) {
    // No match to the current context, so it's
    // Absolute, external URL - just return it in one piece.
    return $matches[0];
  }

  // Remote link should be resolved and absolute now.
  // We've calculated what the real target path is, but that result may very
  // likely be supposed to fall within our wrapper context, so rename it again.
  //
  // If our local context is different from the remote base,
  // eg if we are renaming a wrapped section of a remote site
  // to be something better on our site, (which is common)
  // then substitute the remote path out for our local new path.
  if ($_wrapper_current_path_parts['path'] != '/') {
    $link_parts['path'] = preg_replace('|^' . $_wrapper_instance->base_parts['path'] . '|', $_wrapper_instance->path, $link_parts['path']);
  }

  // Now we know where on the remote location this link really is to
  // But we're not done yet.
  // We want to proxy only content WITHIN the wrapper location,
  // and direct links that fall out of that area probably up-and-over
  // back to the real source.
  // This is no problem if doing a full-site mirror,
  // but may be relevant if only proxying an archive or something.
  // Compare the wrapper base path (local) to the new (local) target path.
  if (empty($_wrapper_instance->base_parts['path']) || strstr($link_parts['path'], $_wrapper_instance->path)) {
    // If A contains B, it's local to this wrapper context and needs capturing.
    // dpm(__FUNCTION__ . " Path that we will be linking *to*
    // ({$link_parts['path']}) *is* within the realm of this wrapper container,
    // [{$_wrapper_instance->base_parts['path']}],
    // replacing it with {$_wrapper_instance->path}");
    // OK, this is to be mirrored inside the current instance - redirect to me
    // And we prepend our local portal path instead.
    unset($link_parts['host']);
    unset($link_parts['port']);
    unset($link_parts['scheme']);
  }
  else {
    // dpm("path {$link_parts['path']} is NOT inside target
    // [{$_wrapper_instance->source_base}]
    // [{$_wrapper_instance->base_parts['path']}]
    // - so it's not ours, it should link back offsite
    // (or up and pretend to be local?");
    $link_parts['host'] = $_wrapper_instance->base_parts['host'];
    if (isset($_wrapper_instance->base_parts['port'])) {
      $link_parts['port'] = $_wrapper_instance->base_parts['port'];
    }
    $link_parts['scheme'] = $_wrapper_instance->base_parts['scheme'];
  }

  // When xpath read the link, it may have 'tidied' the ampersands in queries.
  // $link_parts['query'] = html_entity_decode($link_parts['query']);
  $rewritten = transclusion_glue_url($link_parts);

  return $matches[1] . $rewritten . $matches[3];
}


/**
 * Slightly cleverer parse_url to deal with malformed items.
 *
 * If a link looks broken, try just pegging it to the end of the current
 * context.
 *
 * Some links may appear malformed to parse_url, eg
 * '/cgi-bin/goto/index/2649?http://www.barmixmaster.com/'
 *
 * But we can set them to
 * 'http://webtender.com/cgi-bin/goto/index/2649?http://www.barmixmaster.com/'
 * and continue. (Should technically have been URL-encoded, hence the problem!)
 *
 * If $base is set and some elements are missing from the $link, then transfer
 * parts from the $base into the $link parts
 */
function wrapper_parse_url($link, $base = NULL) {
  $link_parts = @parse_url($link);
  if (!is_array($link_parts)) {
    // Try a messy repair.
    $link_parts = $base;
    if (substr($link_parts['path'], 0, 1) == '/') {
      $link_parts['path'] = $link;
    }
    else {
      $link_parts['path'] = dirname($link_parts['path']) . '/' . $link;
    }
    $newlink = transclusion_glue_url($link_parts);
    // Try again.
    $link_parts = @parse_url($newlink);
    if (!is_array($link_parts)) {
      return NULL;
    }
    // Looks like the patch-up worked.
    // dpm(array('repaired link' => $link_parts));
  }

  if (isset($base)) {
    $base_parts = @parse_url($base);
    $base_parts += array(
      'host' => '',
      'scheme' => '',
      'path' => '',
    );
    if (!isset($link_parts['host'])) {
      $link_parts['host'] = $base_parts['host'];
    }
    if (!isset($link_parts['scheme'])) {
      $link_parts['scheme'] = $base_parts['scheme'];
    }
  }

  return $link_parts;
}

/**
 * Resolve a relative link relative to a given base.
 *
 * Http://nashruddin.com/PHP_Script_for_Converting_Relative_to_Absolute_URL.
 *
 * If the base includes a filename, then that should be discarded.
 * The 'base' is the base directory.
 *
 * wrapper_rel2abs('page.htm', '/folder/subfolder/')
 * => '/folder/subfolder/page.htm'
 *
 * wrapper_rel2abs('page.htm', '/folder/subfolder/index.htm')
 * => '/folder/subfolder/page.htm'
 *
 * wrapper_rel2abs('page.htm', '/folder/subfolder/another.htm')
 * => '/folder/subfolder/page.htm'
 *
 * This means that if the base has no trailing slash, it gets chopped
 *
 * wrapper_rel2abs('page.htm', '/folder/subfolder')
 * => '/folder/page.htm'
 *
 * which may not be what you want, so ensure that folder URLs always end in /
 *
 * Web servers do actually do this for you quietly already.
 *
 * @param string $rel
 *   Relative URL to be resolved.
 * @param string $base
 *   Full URL.
 *
 * @return string
 *   Absoulute URL.
 */
function wrapper_rel2abs($rel, $base) {
  /* return if already absolute URL */
  if (parse_url($rel, PHP_URL_SCHEME) != '') {
    return $rel;
  }

  // No trailing / ? then chop it to find the real base.
  if (!preg_match('%/$%', $base)) {
    $base = dirname($base) . '/';
  }

  /* queries and anchors */
  if ($rel[0] == '#' || $rel[0] == '?') {
    return $base . $rel;
  }

  /* parse base URL and convert to local variables:
  $scheme, $host, $path */
  $scheme = $host = $path = '';
  extract(parse_url($base));

  /* remove non-directory element from path */
  $path = preg_replace('#/[^/]*$#', '', $path);

  /* destroy path if relative url points to root */
  if ($rel[0] == '/') {
    $path = '';
  }

  /* dirty absolute URL */
  $abs = "$host$path/$rel";

  /* replace '//' or '/./' or '/foo/../' with '/' */
  $re = array('#(/\.?/)#', '#/(?!\.\.)[^/]+/\.\./#');
  for ($n = 1; $n > 0; $abs = preg_replace($re, '/', $abs, -1, $n)) {
  }

  /* absolute URL is ready! */
  return $scheme . '://' . $abs;
}
