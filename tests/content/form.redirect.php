<?php
/**
 * @file
 * Sample form that performs a redirect on submission.
 */

if (!empty($_GET)) {
  header('Location: page2.html?' . $_GET['submit']);
}
?>

<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <link rel="stylesheet" type="text/css" href="css/docs.css"/>
  <title>Transclusion Tester : Sample page</title>
</head>
<body>
<h1>Transclusion Tester</h1>

<div id="content">
  <h2>Submit this form to see if REDIRECT works</h2>

  On submission, you should get sent to another page.
  <form action="form.redirect.php" method="GET">
    <input type="text" name="userval" value=""/>
    <input type="hidden" name="hiddenval" value="PASS"/>
    <input type="submit" name="submit" value="REDIRECT THIS"/>
  </form>
</div>
</body>
</html>
