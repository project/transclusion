<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <link rel="stylesheet" type="text/css" href="css/docs.css"/>
  <title>Transclusion Tester : Sample page</title>
</head>
<body>
<h1>Transclusion Tester</h1>

<div id="content">
  <?php

  /**
   * @file
   * Sample form that processes a GET submission.
   */

  if (empty($_GET)) {
    ?>
    <h2>Submit this form to see if GET works</h2>
    <form action="form.get.php" method="GET">
      <input type="text" name="userval" value=""/> <input type="hidden"
                                                          name="hiddenval"
                                                          value="PASS"/> <input
        type="submit" name="submit"
        value="GET THIS"/>
    </form>
  <?php
  }
  else {
    ?>
    <p>A GET submission was received</p>
    <p>
      The user value submitted was
      <?php echo $_GET['userval']; ?>
    </p>
    <p>
      The hidden value submitted was
      <?php echo $_GET['hiddenval']; ?>
    </p>
  <?php
  }
  ?>
  <hr/>
  <a href="subfolder/sub.gif"><img src="subfolder/sub.gif"/></a>
  </li>
  <ul>
    <li><a href=".">Tester page</a></li>
  </ul>
</div>
<div id="footer">
  <a href="subfolder/sub.gif"><img src="subfolder/sub.gif"/></a>
</div>
</body>
</html>
