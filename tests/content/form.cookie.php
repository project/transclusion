<?php
/**
 * @file
 * A submission came from the post form.
 */

if (isset($_POST['userval']) && $_POST['userval'] == "I like cookies") {
  setcookie('transclusion', 'PASS', time() + 60);
  // Expire in 1 minute.
}
?>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <link rel="stylesheet" type="text/css" href="css/docs.css"/>
  <title>Transclusion Tester : Sample page</title>
</head>
<body>
<h1>Transclusion Tester</h1>

<div id="content">
  <h2>Submit this form to see if COOKIES work</h2>

  <form action="form.cookie.php" method="POST">
    <input type="text" name="userval" value="I like cookies"/> <input
      type="submit" name="submit" value="COOKIE THIS"/>
  </form>
  <p>Submitting this once will set a cookie. Submitting it twice will
    detect that a cookie was set. It will expire in 1 minute from setting.</p>
  <?php
  // Check for cookie.
  if (!empty($_COOKIE['transclusion'])) {
    ?>
    <p>
      A cookie was set, it said :
      <?php echo $_COOKIE['transclusion']; ?>
    </p>
  <?php
  }
  else {
    ?>
    <p>
      I am <b>not</b> seeing a cookie yet. FAIL.
    </p>
  <?php
  }
  ?>
  <hr/>
  <a href="subfolder/sub.gif"><img src="subfolder/sub.gif" alt=""/> </a>
  <ul>
    <li><a href=".">Tester page</a></li>
  </ul>

</div>
</body>
</html>
