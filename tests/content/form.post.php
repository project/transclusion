<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <link rel="stylesheet" type="text/css" href="css/docs.css"/>
  <title>Transclusion Tester : Sample page</title>
</head>
<body>
<h1>Transclusion Tester</h1>

<div id="content">

  <?php

  /**
   * @file
   * Sample form that processes a POST.
   */

  if (empty($_POST)) {
    ?>
    <h2>Submit this form to see if POST works</h2>
    <form action="form.post.php" method="POST">
      <input type="text" name="userval" value=""/>
      <input type="hidden" name="hiddenval" value="PASS"/>
      <input type="submit" name="submit" value="POST THIS"/>
    </form>
    <p>I am not seeing any POST data yet ...</p>

  <?php
  }
  else {
    ?>
    <p>A POST submission was received</p>
    <p>The user value submitted was <?php echo $_POST['userval']; ?></p>
    <p>The hidden value submitted was <?php echo $_POST['hiddenval']; ?></p>
  <?php
  }
  ?>

  <hr/>
  <a href="subfolder/sub.gif"><img src="subfolder/sub.gif" alt=""/></a>
  <ul>
    <li><a href=".">Tester page</a></li>
  </ul>

</div>
</body>
</html>
