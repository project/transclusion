<?php
/**
 * @file
 * Transclusion_entity.features.field_instance.inc.
 */

/**
 * Implements hook_field_default_field_instances().
 */
function transclusion_entity_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance:
  // 'field_collection_item-field_transclusion-cache_control'
  $field_instances['field_collection_item-field_transclusion-cache_control'] = array(
    'bundle' => 'field_transclusion',
    'default_value' => array(
      0 => array(
        'value' => 'public',
      ),
    ),
    'deleted' => 0,
    'description' => 'Based on the HTTP Cache-control directives, The remote content can be refreshed never, sometimes or always',
    'display' => array(
      'default' => array(
        'label' => 'inline',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 5,
      ),
    ),
    'entity_type' => 'field_collection_item',
    'field_name' => 'cache_control',
    'label' => 'Cache-control',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_select',
      'weight' => 7,
    ),
  );

  // Exported field_instance:
  // 'field_collection_item-field_transclusion-cache_control_max_age'
  $field_instances['field_collection_item-field_transclusion-cache_control_max_age'] = array(
    'bundle' => 'field_transclusion',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'If choosing to periodically update the content, choose the expiry time.
If a refresh fails, the old content will be kept, and the refresh will be tried again next time. If not using periodic cache control, this value if not used.
',
    'display' => array(
      'default' => array(
        'label' => 'inline',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 6,
      ),
    ),
    'entity_type' => 'field_collection_item',
    'field_name' => 'cache_control_max_age',
    'label' => 'Max-age',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_select',
      'weight' => 8,
    ),
  );

  // Exported field_instance:
  // 'field_collection_item-field_transclusion-content'
  $field_instances['field_collection_item-field_transclusion-content'] = array(
    'bundle' => 'field_transclusion',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'The cached content in its raw or parsed form. Not generally re-rendered yet, though in simpler cases just treating it with an output format is enough.',
    'display' => array(
      'default' => array(
        'label' => 'inline',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 1,
      ),
    ),
    'entity_type' => 'field_collection_item',
    'field_name' => 'content',
    'label' => 'Content',
    'required' => 0,
    'settings' => array(
      'text_processing' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 5,
      ),
      'type' => 'text_textarea',
      'weight' => 3,
    ),
  );

  // Exported field_instance:
  // 'field_collection_item-field_transclusion-dateaccessed'
  $field_instances['field_collection_item-field_transclusion-dateaccessed'] = array(
    'bundle' => 'field_transclusion',
    'deleted' => 0,
    'description' => 'The timestamp when this resource was last checked for freshness. This is neither the date of retrieval nor the date of original publication, but an internal counter used for auditing.',
    'display' => array(
      'default' => array(
        'label' => 'inline',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 3,
      ),
    ),
    'entity_type' => 'field_collection_item',
    'field_name' => 'dateaccessed',
    'label' => 'Date accessed',
    'required' => 0,
    'settings' => array(
      'default_value' => 'blank',
      'default_value2' => 'same',
      'default_value_code' => '',
      'default_value_code2' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'date',
      'settings' => array(
        'increment' => 15,
        'input_format' => 'Y-m-d H:i:s',
        'input_format_custom' => '',
        'label_position' => 'above',
        'text_parts' => array(),
        'year_range' => '-3:+3',
      ),
      'type' => 'date_text',
      'weight' => 4,
    ),
  );

  // Exported field_instance:
  // 'field_collection_item-field_transclusion-datepublished'
  $field_instances['field_collection_item-field_transclusion-datepublished'] = array(
    'bundle' => 'field_transclusion',
    'deleted' => 0,
    'description' => 'The publication date of the remote resource, near as can be determined when it is retrieved. This is not expected to change unless the remote content does also.
This date is supplied by the remote document or server, and may predate our date of retrieval.
If the remote date cannot be determined, it is set to the date of first retrieval. 
This is the date you most commonly want to display in a citation.',
    'display' => array(
      'default' => array(
        'label' => 'inline',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 4,
      ),
    ),
    'entity_type' => 'field_collection_item',
    'field_name' => 'datepublished',
    'label' => 'Date of publication',
    'required' => 0,
    'settings' => array(
      'default_value' => 'blank',
      'default_value2' => 'same',
      'default_value_code' => '',
      'default_value_code2' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'date',
      'settings' => array(
        'increment' => 15,
        'input_format' => 'Y-m-d H:i:s',
        'input_format_custom' => '',
        'label_position' => 'above',
        'text_parts' => array(),
        'year_range' => '-3:+3',
      ),
      'type' => 'date_text',
      'weight' => 6,
    ),
  );

  // Exported field_instance:
  // 'field_collection_item-field_transclusion-dateretrieved'
  $field_instances['field_collection_item-field_transclusion-dateretrieved'] = array(
    'bundle' => 'field_transclusion',
    'deleted' => 0,
    'description' => 'Last date when this resource was successfully retrieved and modified. Retrievals that produce no change do not increment this date.',
    'display' => array(
      'default' => array(
        'label' => 'inline',
        'module' => 'date',
        'settings' => array(
          'format_type' => 'short',
          'fromto' => 'both',
          'multiple_from' => '',
          'multiple_number' => '',
          'multiple_to' => '',
        ),
        'type' => 'date_default',
        'weight' => 2,
      ),
    ),
    'entity_type' => 'field_collection_item',
    'field_name' => 'dateretrieved',
    'label' => 'Date retrieved',
    'required' => 0,
    'settings' => array(
      'default_value' => 'blank',
      'default_value2' => 'same',
      'default_value_code' => '',
      'default_value_code2' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'date',
      'settings' => array(
        'increment' => 15,
        'input_format' => 'Y-m-d H:i:s',
        'input_format_custom' => '',
        'label_position' => 'above',
        'text_parts' => array(),
        'year_range' => '-3:+3',
      ),
      'type' => 'date_text',
      'weight' => 5,
    ),
  );

  // Exported field_instance:
  // 'field_collection_item-field_transclusion-extraction_method'
  $field_instances['field_collection_item-field_transclusion-extraction_method'] = array(
    'bundle' => 'field_transclusion',
    'default_value' => array(
      0 => array(
        'value' => 'auto',
      ),
    ),
    'deleted' => 0,
    'description' => 'How to extract the remote content from the referred resource.
If linking to a section of a page that has a heading, use an html #anchor, eg http://example.com/page2.html#section2-1 . This will try to bring in all content FOLLOWING a given heading until the next heading of equal or greater size.
If linking to an element in the page that has a useful ID on a containing element, use ID method, eg http://example.com/page2.html#synopsis . 
XPath or querypath can be used to target harder-to reach places, like the third paragraph in the element labeled article : //[@id=\'article\']//p[3]',
    'display' => array(
      'default' => array(
        'label' => 'inline',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 7,
      ),
    ),
    'entity_type' => 'field_collection_item',
    'field_name' => 'extraction_method',
    'label' => 'Extraction method',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_select',
      'weight' => 1,
    ),
  );

  // Exported field_instance:
  // 'field_collection_item-field_transclusion-extraction_settings'
  $field_instances['field_collection_item-field_transclusion-extraction_settings'] = array(
    'bundle' => 'field_transclusion',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Internal use only. This will have different, serialized values depending on what extraction method is used.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 8,
      ),
    ),
    'entity_type' => 'field_collection_item',
    'field_name' => 'extraction_settings',
    'label' => 'Extraction settings',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
      'wysiwyg_fields' => array(
        'settings' => array(
          'advanced' => array(
            'delete' => 1,
            'hide' => 1,
          ),
          'formatters' => array(),
          'icon' => '6f7621a02161e347e66d99d8ace540a3',
          'label' => 'Extraction settings',
        ),
        'status' => 0,
      ),
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 5,
      ),
      'type' => 'text_textarea',
      'weight' => 2,
    ),
  );

  // Exported field_instance:
  // 'field_collection_item-field_transclusion-source'
  $field_instances['field_collection_item-field_transclusion-source'] = array(
    'bundle' => 'field_transclusion',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'inline',
        'module' => 'link',
        'settings' => array(),
        'type' => 'link_url',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'field_collection_item',
    'field_name' => 'source',
    'label' => 'Source',
    'required' => 1,
    'settings' => array(
      'attributes' => array(
        'class' => '',
        'configurable_title' => 0,
        'rel' => 'cite',
        'target' => 'default',
        'title' => '',
      ),
      'display' => array(
        'url_cutoff' => 80,
      ),
      'enable_tokens' => 1,
      'rel_remove' => 'default',
      'title' => 'none',
      'title_maxlength' => 128,
      'title_value' => '',
      'url' => 0,
      'user_register_form' => FALSE,
      'validate_url' => 0,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'link',
      'settings' => array(),
      'type' => 'link_field',
      'weight' => 0,
    ),
  );

  // Exported field_instance:
  // 'node-wrapper-body'
  $field_instances['node-wrapper-body'] = array(
    'bundle' => 'wrapper',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(
          'trim_length' => 600,
        ),
        'type' => 'text_summary_or_trimmed',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'body',
    'label' => 'Body',
    'required' => FALSE,
    'settings' => array(
      'display_summary' => TRUE,
      'text_processing' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'module' => 'text',
      'settings' => array(
        'rows' => 20,
        'summary_rows' => 5,
      ),
      'type' => 'text_textarea_with_summary',
      'weight' => -4,
    ),
  );

  // Exported field_instance:
  // 'node-wrapper-field_transclusion'
  $field_instances['node-wrapper-field_transclusion'] = array(
    'bundle' => 'wrapper',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Configurations for the remote target and how to deal with content from there.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'transclusion',
        'settings' => array(
          'cache_control' => 'default',
        ),
        'type' => 'transcluded',
        'weight' => 1,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_transclusion',
    'label' => 'Transclusion',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
      'wysiwyg_fields' => array(
        'settings' => array(
          'advanced' => array(
            'delete' => 1,
            'hide' => 1,
          ),
          'formatters' => array(),
          'icon' => '6f7621a02161e347e66d99d8ace540a3',
          'label' => 'transclusion',
        ),
        'status' => 0,
      ),
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'field_collection',
      'settings' => array(),
      'type' => 'field_collection_embed',
      'weight' => -2,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Based on the HTTP Cache-control directives, The remote content can be refreshed never, sometimes or always');
  t('Body');
  t('Cache-control');
  t('Configurations for the remote target and how to deal with content from there.');
  t('Content');
  t('Date accessed');
  t('Date of publication');
  t('Date retrieved');
  t('Extraction method');
  t('Extraction settings');
  t('How to extract the remote content from the referred resource.
If linking to a section of a page that has a heading, use an html #anchor, eg http://example.com/page2.html#section2-1 . This will try to bring in all content FOLLOWING a given heading until the next heading of equal or greater size.
If linking to an element in the page that has a useful ID on a containing element, use ID method, eg http://example.com/page2.html#synopsis . 
XPath or querypath can be used to target harder-to reach places, like the third paragraph in the element labeled article : //[@id=\'article\']//p[3]');
  t('If choosing to periodically update the content, choose the expiry time.
If a refresh fails, the old content will be kept, and the refresh will be tried again next time. If not using periodic cache control, this value if not used.
');
  t('Internal use only. This will have different, serialized values depending on what extraction method is used.');
  t('Last date when this resource was successfully retrieved and modified. Retrievals that produce no change do not increment this date.');
  t('Max-age');
  t('Source');
  t('The cached content in its raw or parsed form. Not generally re-rendered yet, though in simpler cases just treating it with an output format is enough.');
  t('The publication date of the remote resource, near as can be determined when it is retrieved. This is not expected to change unless the remote content does also.
This date is supplied by the remote document or server, and may predate our date of retrieval.
If the remote date cannot be determined, it is set to the date of first retrieval. 
This is the date you most commonly want to display in a citation.');
  t('The timestamp when this resource was last checked for freshness. This is neither the date of retrieval nor the date of original publication, but an internal counter used for auditing.');
  t('Transclusion');

  return $field_instances;
}
