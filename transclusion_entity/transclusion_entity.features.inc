<?php
/**
 * @file
 * Transclusion_entity.features.inc.
 */

/**
 * Implements hook_node_info().
 */
function transclusion_entity_node_info() {
  $items = array(
    'wrapper' => array(
      'name' => t('Wrapper'),
      'base' => 'node_content',
      'description' => t('Content included from elsewhere and presented as a page or a section inside this site.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
