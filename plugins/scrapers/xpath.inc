<?php
/**
 * @file
 * Document scraper method plugin.
 */

// Plugin definition.
$plugin = array(
  'title' => t('Xpath'),
  'description' => t('Use XML parsing and XPath rules to identify the content.'),
  // Name the scraper callback.
  'callback' => 'xpath_scraper',
  // Name the config form.
  'form' => 'xpath_scraper_form',
  // Default config settings.
  'default conf' => array(
    'xpath' => '//body',
  ),
  'weight' => 1,
);


/**
 * Run a xpath-based content extraction on the given source.
 *
 * BUG? Some input produces an unwanted number of encoded carriage returns
 * (&#13; or &#xD;) once XML DOM has imported it and returned it. I'm stripping
 * these out in a pre-precess step, but there is probably an encoding directive
 * that can stop this from happening.
 *
 * @param object $settings
 *   The static configurations for scraping.
 * @param array $context
 *   The per-request calculated data and stuff like request
 *   headers and relative path.
 *   It includes the raw source, and will have the parsed body added to it.
 *
 * @return bool
 *   Indicates success. Sometimes empty string is a success,
 *   but a parse error is not.
 */
function xpath_scraper($settings, array &$context) {
  // $source, $wrapper, &$node.
  $pattern = $settings->extraction_settings['xpath'];
  if (empty($pattern)) {
    // May be a new or unconfigured node.
    watchdog('wrapper', 'No xpath pattern provided', array(), WATCHDOG_WARNING);
    drupal_set_message(t('No xpath pattern provided'), 'warning');
    return NULL;
  }

  // Preprocess cleanups.
  // Any number of patterns may be found and pulled out of the flow.
  // Store them in the context for later.
  // If you don't want to use them (like breadcrumbs) don't print them later.
  // Needed Scripts and css with regexp because xpath did not co-operate with
  // CDATA and nested comments
  // ... as seen in conformant xhtml.
  $regex_extractions = array(
    'css' => '%<style[\S\s]*?</style>|<link type="text/css" rel="stylesheet".*href="(.*)".*/>%',
    'js' => '%<script[\S\s]*?</script>|<script.*src="(.*)".*></script>%',
  );
  foreach ($regex_extractions as $key => $extraction_pattern) {
    $context[$key] = array();
    preg_match_all($extraction_pattern, $context['source'], $matches);
    foreach ($matches[0] as $match) {
      $context[$key][] = $match;
    }
    // Also now discard this from the original.
    $context['source'] = preg_replace($extraction_pattern, '', $context['source']);
  }

  // Other sane stuff works better as XML and XPath, and it's the preferred
  // method.
  $dom = new DOMDocument();
  $success = @$dom->loadHTML($context['source']);
  if (!$success) {
    watchdog('wrapper', 'There was an error parsing the HTML document for xpath extraction', array(), WATCHDOG_ERROR);
    return NULL;
  }
  $xpath = new DOMXpath($dom);

  $xpath_extractions = array(
    'title' => $settings->extraction_settings['title_xpath'],
    'breadcrumbs' => '//*[@class="breadcrumb"]',
  );
  foreach ($xpath_extractions as $key => $extraction_pattern) {
    $context[$key] = array();
    $extraction = @$xpath->query($extraction_pattern);
    if (!empty($extraction)) {
      foreach ($extraction as $element) {
        $context[$key][] = $dom->saveXML($element);
        $element->parentNode->removeChild($element);
      }
    }
  }

  // If innerhtml option is set, do not return the element,
  // return the *content* of the element.
  if (!empty($settings->extraction_settings['innerhtml'])) {
    // $pattern .= '/*|text()';
    // $pattern = $pattern . '/*' . '|' . $pattern . '/text()';
    $pattern .= '/node()';
  }

  $all_matches = @$xpath->query($pattern);
  $parsed_items = array();
  if (!is_null($all_matches)) {
    // I expect one element, but maybe join all the valid results.
    foreach ($all_matches as $element) {
      $parsed_items[] = $dom->saveXML($element);
    }
  }
  if (empty($parsed_items)) {
    $node = $context['node'];
    // TODO Check this $node is still correct.
    watchdog('wrapper', 'XPath pattern %pattern did not match anything within source for %path', array('%pattern' => $pattern, '%path' => $node->path), WATCHDOG_ERROR);
  }

  $output = implode("\n", $parsed_items);

  // TODO - this should be made configurable OR sort out the xml issues.
  if (!empty($context['js'])) {
    $output .= implode("\n", $context['js']);
  }

  if ($output) {
    $context['body'] = $output;
    return TRUE;
  }

  return FALSE;
}

/**
 * Settings form definition. Gets inserted into the field edit space.
 *
 * Per-scraper min-forms get their data serialized for storage and unpacked
 * again later - this is because each widget may have arbitrary settings,
 * but field module will just store it in a blob.
 * The blob is serialized on validate by the parent form.
 *
 * @param array $settings
 *   Simple values as defined by the form.
 *   These settings may have been serialized and deserialized since we last
 *   saw them.
 * @param object $transclusion_entity_wrapper
 *   Handle on the current data object.
 *
 * @return array
 *   FAPI form.
 */
function xpath_scraper_form($settings = array(), $transclusion_entity_wrapper) {

  $form = array();
  $form['xpath'] = array(
    '#title' => t('XPath'),
    '#type' => 'textfield',
    '#default_value' => @$settings['xpath'],
  );
  $form['innerhtml'] = array(
    '#title' => t('Inner HTML'),
    '#type' => 'checkbox',
    '#default_value' => @$settings['innerhtml'],
    '#description' => t('If set, Include only the <em>content</em> of the found element - discarding the wrapper div.'),
  );
  $form['title_xpath'] = array(
    '#title' => t('Title XPath'),
    '#type' => 'textfield',
    '#default_value' => @$settings['title_xpath'],
    '#description' => t('eg "//h1". If set, seach for a title matching this pattern in the page, remove it from the body and keep it for rendering elsewhere - such as the Drupal page title.'),
  );
  return $form;
}
