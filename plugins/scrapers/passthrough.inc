<?php
/**
 * @file
 * Document scraper method plugin.
 */

// Plugin definition.
$plugin = array(
  'title' => t('Passthrough'),
  'description' => t('No processing. Display the result as-is.'),
  // Name the scraper callback.
  'callback' => 'passthrough_scraper',
);

/**
 * Return the result raw.
 *
 * Should never actually get here
 * as the pipeline should have diverted before now.
 */
function passthrough_scraper($settings, &$context, &$result) {
  $result['output'] = $result['raw'];
  return TRUE;
}
