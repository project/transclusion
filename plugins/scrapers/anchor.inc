<?php
/**
 * @file
 * Document scraper method plugin.
 *
 * See document examples in tests/content/page2 for the types of content
 * sections we expect to extract.
 */

// Plugin definition.
$plugin = array(
  'title' => t('Anchor'),
  'description' => t('Find the named section of a page.'),
  // Name the scraper callback.
  'callback' => 'anchor_scraper',
  // Name the config form.
  'form' => 'anchor_scraper_form',
  // Default config settings.
  'default conf' => array(
    'anchor' => 'content',
    'following' => 1,
  ),
  'weight' => 1,
);


/**
 * Run a semantic content extraction on the given source.
 *
 * @param object $settings
 *   The static configurations for scraping.
 *   When the context is a node, this is shaped like a node object.
 *   - extraction_settings array.
 *   - source_base.
 *   - cache_control.
 *   - extraction_method.
 * @param array $context
 *   The per-request calculated data and stuff like request
 *   headers and relative path.
 *   It includes the raw source, and will have the parsed body added to it.
 *   - source HTML.
 *   - source_path url.
 *   - dest_path local path.
 *   - response array of headers.
 *
 * @return bool
 *   Indicates success. Sometimes empty string is a success,
 *   but a parse error is not.
 */
function anchor_scraper($settings, array &$context) {
  // $source, $wrapper, &$node.
  // The anchor or name in the page should be indicated by a #id in the URL.
  $url_parts = parse_url($settings->source);
  if (!empty($url_parts['fragment'])) {
    $fragment = $url_parts['fragment'];
    $pattern = "//*[@id='$fragment']|//*[@name='$fragment']";
  }
  else {
    // May be a new or unconfigured node.
    watchdog('wrapper', 'No anchor provided', array(), WATCHDOG_WARNING);
    drupal_set_message(t('No anchor provided'), 'warning');
    $pattern = "//<body>";
  }

  // Other sane stuff works better as XML and XPath, and it's the preferred
  // method.
  $dom = new DOMDocument();
  $success = @$dom->loadHTML($context['source']);
  if (!$success) {
    watchdog('wrapper', 'There was an error parsing the HTML document for xpath extraction', array(), WATCHDOG_ERROR);
    return NULL;
  }
  $xpath = new DOMXpath($dom);

  $all_matches = @$xpath->query($pattern);
  $parsed_items = array();
  if (!is_null($all_matches)) {
    // I expect one element, but maybe join all the valid results.
    foreach ($all_matches as $element) {
      // If the item we found is a heading or an a on it's own, we are probably
      // going to have to keep grabbing the rest of the document as far as
      // we can.
      $h_tags = array(1 => 'h1', 2 => 'h2', 3 => 'h3', 4 => 'h4', 5 => 'h5');
      $tagname = $element->tagName;
      if (in_array($tagname, $h_tags)) {
        $section = anchor_extract_chunk_below($element);
        $parsed_items[] = $dom->saveXML($section);
      }
      if (in_array($tagname, array('a'))) {
        // This is likely to infer that the subsequent content
        // is the real target.
        // But it's tricky.
      }

    }
  }

  if (empty($parsed_items)) {
    $node = $context['node'];
    // TODO Check this $node is still correct.
    watchdog('wrapper', 'XPath pattern %pattern did not match anything within source for %path', array('%pattern' => $pattern, '%path' => $node->path), WATCHDOG_ERROR);
  }

  $output = implode("\n", $parsed_items);

  if ($output) {
    $context['body'] = $output;
    return TRUE;
  }

  return FALSE;
}


/**
 * Extract the portion of a document that makes up a named 'section'.
 *
 * This will wrap all content *following* a heading of a given depth into a
 * content block container, and the heading+content into a block of its own
 * that binds the two together. This is a section.
 *
 * @param DOMNode $header
 *   The h tag that marks the start of a section.
 *
 * @return DOMDocumentFragment
 *   The contents of the section.
 */
function anchor_extract_chunk_below(DOMNode $header) {
  $scope = $header->parentNode;
  $dom = $scope->ownerDocument;
  $chunk = $dom->createDocumentFragment();

  $h_tags = array(1 => 'h1', 2 => 'h2', 3 => 'h3', 4 => 'h4', 5 => 'h5');
  $tagname = $header->tagName;
  $start_level = array_search($tagname, $h_tags);

  if ($start_level) {
    // It's a heading.
    // These are likely to infer that the *subsequent* content
    // is the real target.
    // Iterate the *following* nodes until you find a heading of the same
    // or higher weight.
    // Need to make sure I don't chunk above my requested level.
    // Prepare a list of things that should break my flow.
    $higher_tags = array();
    for ($t = $start_level - 1; $t > 0; $t--) {
      $higher_tags["h$t"] = "h$t";
    }

    $chunk->appendChild($header);
    $thisnode = $header->nextSibling;
    while ($thisnode) {
      if (!empty($higher_tags[$thisnode->nodeName])) {
        // If I find higher tags than expected, stop what I'm doing and
        // close the current container.
        unset($thisnode);
        continue;
      }
      // Otherwise, tag the found content onto the new fragment.
      $chunk->appendChild($thisnode);
      $thisnode = $thisnode->nextSibling;
    }
  }
  else {
    // Not a h tag.
  }
  return $chunk;
}

/**
 * Settings form definition. Gets inserted into the field edit space.
 *
 * Per-scraper min-forms get their data serialized for storage and unpacked
 * again later - this is because each widget may have arbitrary settings,
 * but field module will just store it in a blob.
 * The blob is serialized on validate by the parent form.
 *
 * @param array $settings
 *   Simple values as defined by the form.
 *   These settings may have been serialized and deserialized since we last
 *   saw them.
 * @param object $transclusion_entity_wrapper
 *   Handle on the current data object.
 *
 * @return array
 *   FAPI form.
 */
function anchor_scraper_form($settings = array(), $transclusion_entity_wrapper) {

  $form = array();
  $form['following'] = array(
    '#title' => t('Following'),
    '#type' => 'checkbox',
    '#default_value' => $settings['following'],
    '#description' => t('If the element you are targeting is not a content wrapper, maybe you want to select everything from this point down to the next anchor.'),
  );
  return $form;
}
